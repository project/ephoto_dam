<?php

namespace Drupal\ephoto_dam\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the plugin.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ephoto_dam.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ephoto_dam_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ephoto_dam.settings');

    $form['settings_server_url'] = [
      '#title' => $this->t('Server URL'),
      '#type' => 'url',
      '#description' => $this->t('Please enter the URL of your Ephoto Dam software'),
      '#default_value' => $config->get('server_url'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => $this->t('https://ephoto.mycompany.com/')],
      '#size' => 75,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $server_url = $form_state->getValue('settings_server_url');

    $server_url = filter_var($server_url, FILTER_VALIDATE_URL);

    if (!$server_url) {
      $form_state->setErrorByName('settings_server_url', $this->t('Please enter a correct URL'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $server_url = $form_state->getValue('settings_server_url');

    if (substr($server_url, -1) !== '/') {
      $server_url .= '/';
    }

    $this->config('ephoto_dam.settings')
      ->set('server_url', $server_url)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
