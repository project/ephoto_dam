<?php

namespace Drupal\ephoto_dam\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\editor\EditorInterface;

/**
 * Defines the plugin.
 *
 * @CKEditor5Plugin(
 *   id = "ephoto_dam_simplebox",
 *   ckeditor5 = @CKEditor5AspectsOfCKEditor5Plugin(
 *     plugins = { "EphotoDam" },
 *   ),
 *   drupal = @DrupalAspectsOfCKEditor5Plugin(
 *     label = @Translation("Ephoto Dam"),
 *   ),
 * )
 */
class EphotoDam extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface {
  use CKEditor5PluginConfigurableTrait;

  private const DEFAULT_SETTINGS = [
    'server_url' => '',
    'captions' => TRUE,
    'captions_format' => '',
    'zoom' => TRUE,
    'images_size' => 320,
    'videos_size' => 360,
    'documents_size' => 320,
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return self::DEFAULT_SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'ephoto_dam/settings';

    $form['captions'] = [
      '#title' => $this->t('Captions'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['captions'],
      '#description' => $this->t('Display the caption under images, videos and documents'),
      '#required' => FALSE,
    ];

    $form['captions_format'] = [
      '#title' => $this->t('Captions format'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['captions_format'],
      '#required' => FALSE,
    ];

    $form['zoom'] = [
      '#title' => $this->t('Zoom'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['zoom'],
      '#description' => $this->t('View large images by clicking on them'),
      '#required' => FALSE,
    ];

    $form['images_size'] = [
      '#title' => $this->t('Images size'),
      '#type' => 'number',
      '#default_value' => $this->configuration['images_size'],
      '#description' => $this->t('Please enter the size of the images for the display'),
      '#required' => TRUE,
      '#field_suffix' => 'px',
      '#attributes' => ['min' => 0, 'max' => 6000],
      '#size' => 20,
    ];

    $form['videos_size'] = [
      '#title' => $this->t('Videos size'),
      '#type' => 'number',
      '#default_value' => $this->configuration['videos_size'],
      '#description' => $this->t('Please enter the size of the videos for the display'),
      '#required' => TRUE,
      '#field_suffix' => 'px',
      '#attributes' => ['min' => 0, 'max' => 6000],
      '#size' => 20,
    ];

    $form['documents_size'] = [
      '#title' => $this->t('Documents size'),
      '#type' => 'number',
      '#default_value' => $this->configuration['documents_size'],
      '#description' => $this->t('Please enter the size of the documents for the display'),
      '#required' => TRUE,
      '#field_suffix' => 'px',
      '#attributes' => ['min' => 0, 'max' => 6000],
      '#size' => 20,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    $this->configuration['captions'] = $form_state
      ->getValue('captions');

    $this->configuration['captions_format'] = $form_state
      ->getValue('captions_format');

    $this->configuration['zoom'] = $form_state
      ->getValue('zoom');

    $this->configuration['images_size'] = $form_state
      ->getValue('images_size');

    $this->configuration['videos_size'] = $form_state
      ->getValue('videos_size');

    $this->configuration['documents_size'] = $form_state
      ->getValue('documents_size');

  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form_value = $form_state
      ->getValue('captions');
    $form_state
      ->setValue('captions', $form_value);

    $form_value = $form_state
      ->getValue('captions_format');
    $form_state
      ->setValue('captions_format', $form_value);

    $form_value = $form_state
      ->getValue('zoom');
    $form_state
      ->setValue('zoom', $form_value);

    $form_value = $form_state
      ->getValue('images_size');
    $form_state
      ->setValue('images_size', $form_value);

    $form_value = $form_state
      ->getValue('videos_size');
    $form_state
      ->setValue('videos_size', $form_value);

    $form_value = $form_state
      ->getValue('documents_size');
    $form_state
      ->setValue('documents_size', $form_value);

  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {

    $config = $this->getConfiguration();

    $dynamic_plugin_config = $static_plugin_config;


    $dynamic_plugin_config['ephoto_dam'] =  [
      'captions' => $config['captions'],
      'captions_format' => $config['captions_format'],
      'zoom' => $config['zoom'],
      'images_size' => $config['images_size'],
      'videos_size' => $config['videos_size'],
      'documents_size' => $config['documents_size'],
      'align' => "none",
    ];

    
    if (!isset($dynamic_plugin_config['image']['caption']['isEnabled'])) {
      $dynamic_plugin_config['image']['caption']['isEnabled'] = true;
    }
    if (!isset($dynamic_plugin_config['image']['caption']['defaultText'])) {
      $dynamic_plugin_config['image']['caption']['defaultText'] = 'test a';
    }


    /*
    // Dynamically set the configuration to enable captions by default.
    if (isset($dynamic_plugin_config['image'])) {
      $dynamic_plugin_config['image']['toolbar'] = ['toggleImageCaption'];
      $dynamic_plugin_config['image']['caption'] = [
        'isEnabled' => true,  // Enable captions by default.
      ];
    } else {
      // If 'image' is not already configured, initialize it.
      $dynamic_plugin_config['image'] = [
        'toolbar' => ['toggleImageCaption'],
        'caption' => [
          'isEnabled' => true,  // Enable captions by default.
        ],
      ];
    }*/

    return $dynamic_plugin_config;

  }

}
