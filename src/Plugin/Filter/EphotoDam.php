<?php

namespace Drupal\ephoto_dam\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DOMDocument;

/**
 * Defines the plugin.
 *
 * @Filter(
 *   title = @Translation("Ephoto Dam"),
 *   id = "ephoto_dam_2",
 *   description = @Translation("Activate the use of the Ephoto Dam filter"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class EphotoDam extends FilterBase implements ContainerFactoryPluginInterface
{

    /**
     * Default settings.
     */
    private const DEFAULT_SETTINGS = [
        'ephoto_dam',
        'align',
        'src',
        'embed',
        'width',
        'height',
        'alt',
        'captions',
        'captions_format',
        'zoom',
    ];

    /**
     * Ephoto_dam constructor.
     *
     * @param array $configuration
     *   Plugin configuration.
     * @param string $plugin_id
     *   Plugin ID.
     * @param mixed $plugin_definition
     *   Plugin definition.
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static($configuration, $plugin_id, $plugin_definition);
    }

    /**
     * {@inheritdoc}
     */
    public function process($text, $langcode)
    {


        $text = mb_convert_encoding($text, 'ISO-8859-1', 'UTF-8');
        $text = $this->replace($text);
        $result = new FilterProcessResult($text);
        $doc = new DOMDocument();


        @$doc->loadHTML($text);
        $finder = new \DomXPath($doc);
        $classname = "ephoto_dam_iframe";
        $text = $doc->saveXML($doc);


        foreach ($this->getValidMatches($text) as $source => $values) {


            if (isset($values['ephoto_dam'])) {


                /////////////////////////////////////////////
                $docSource = new DOMDocument();
                @$docSource->loadHTML($source);
                $finderSource = new \DomXPath($docSource);
                $classcaptionname = "caption";
                $nodesCaption = $finderSource->query("//*[contains(@class, '$classcaptionname')]");

                if ($nodesCaption) {
                    $nodeCaption = $nodesCaption[0];
                    if ($nodeCaption && $nodeCaption->textContent) {
                        $filtered_caption = str_replace('Â', '', $nodeCaption->textContent);
                        $filtered_caption = str_replace('&nbsp;', '', $filtered_caption);
                        $values['captions'] = mb_convert_encoding($filtered_caption, "ISO-8859-1","UTF-8");
                    }
                }

                /////////////////////////////////////////////

                if ($values['ephoto_dam'] === 'image') {
                    $html = $this->renderImage((object) $values);
                } else {
                    $html = $this->renderEmbed((object) $values);
                }

                $id_div = $values['id_div'];

                if(isset($values['captions']) && $values['captions']){
                    $pattern = '/<figure\b[^>]*>(?:(?!<\/figure>).)*?<[^>]*id=["\']'.$id_div.'["\'][^>]*>(?:(?!<\/figure>).)*<\/figure>/';
                }
                else{
                    $pattern = '/<p\b[^>]*>(?:(?!<\/p>).)*?<[^>]*id=["\']'.$id_div.'["\'][^>]*>(?:(?!<\/p>).)*<\/p>/';
                }


                $text = preg_replace($pattern, $html, $text);
                $result->setAttachments(['library' => ['ephoto_dam/display']]);


            }
        }



        $result->setProcessedText($text);

        return $result;
    }

    /**
     * Render Image code html.
     *
     * @param object $values
     *   Values of item.
     *
     * @return string
     *   Return image.
     */
    private function renderImage($values)
    {

        $html = '';

        if (isset($values->captions) && $values->captions) {
            $html .= '<figure class="caption caption-img ephoto-dam-align-' . $values->align . '" role="group">';
        }

        $html .= '<article class="article_ephoto_dam media media--type-image media--view-mode-responsive-3x2 ephoto-dam-align-' . $values->align . '" style="width:' . $values->width . 'px">';
        $html .= '<img';
        $html .= ' alt="' . $values->alt . '"';
        $html .= ' src="' . $values->src . '"';

        if ($values->ephoto_dam == "Text") {
            $values->width = $values->width * 2;
        }

        $html .= 'style = width:' . $values->width . 'px;height:' . "auto" . '';

        $values->zoom = ($values->zoom === "true");
        $html .= ' class="' . ($values->zoom ? 'ephoto-dam-zoom' : '');
        $html .= '">';

        if (isset($values->captions) && $values->captions) {
            $html .= '<figcaption class="ephoto_dam_caption">' . $values->captions . '</figcaption>';
        }

        $html .= '</article>';


        if (isset($values->captions) && $values->captions) {
            $html .= '</figure>';
        }


        return $html;
    }

    /**
     * Render Embed code html.
     *
     * @param object $values
     *   Values of item.
     *
     * @return string
     *   Embeded media.
     */
    private function renderEmbed($values)
    {


        $html = '';

        if (isset($values->captions) && $values->captions) {
            $html .= '<figure class="caption caption-img ephoto-dam-align-' . $values->align . '" role="group">';
        }

        $html .= '<div class="article">';
        $html .= '<div style="width:' . $values->width . 'px; min-width:' . $values->width . 'px" class="ephoto_iframe align-' . $values->align . '">';
        $html .= '<iframe scrolling="no" allow="autoplay" allowfullscreen="true" frameborder="0"';
        $html .= ' alt="' . $values->alt . '"';
        $html .= ' src="' . $values->embed . '"';

        if (isset($values->newHeight) && $values->newHeight != NULL) {
            $html .= ' height="' . $values->newHeight . '"';
        } else {
            $html .= ' height="' . $values->oldHeight . '"';
        }

        $html .= ' width="' . $values->width . '"';
        $html .= ' title="' . "test" . '"';
        $html .= '></iframe>';

        if (isset($values->captions) && $values->captions) {
            $html .= '<figcaption class="ephoto_dam_caption">' . $values->captions . '</figcaption>';
        }

        /*
        $html .= "</div>";
        $html .= "</div>";
        */
        $html .= '</figure>';

        return $html;
    }

    /**
     * Get all valid matches in the WYSIWYG.
     *
     * @param string $text
     *   The text to check for WYSIWYG matches.
     *
     * @return mixed
     *   An array of data from the text keyed by the text content.
     */
    protected function getValidMatches($text)
    {

        $return_valid_matches = [];

        $doc = new DOMDocument();
        @$doc->loadHTML($text);
        $finder = new \DomXPath($doc);
        $classname = "ckeditor_ephoto_dam";
        $nodes = $finder->query("//*[contains(@class, '$classname')]");
        $i = 1;


        while ($i < $nodes->length + 1) {

            $node = $nodes[$i - 1];


            if($node->firstElementChild){
                $img_node = $node->firstElementChild;
            }
            else{
                $img_node = $node;
            }

            $nodeString = $node->ownerDocument->saveXML($img_node);

            $globalDivToReturn = $nodeString;
            $data = $img_node->getAttribute("data");


            $data = str_replace("'", "\"", $data);
            $json = json_decode($data, true);




            $array_value_tag = [
                "ephoto_dam",
                "src",
                "alt",
                "zoom",
                "oldwidth",
                "oldHeight",
                "embed",
                "width",
            ];

            $matches = [];
            $matches['id_div'] = $img_node->getAttribute("id");

            foreach ($array_value_tag as $value_tag) {

                if (isset($json[$value_tag])) {
                    $matches[$value_tag] = $json[$value_tag];
                }
            }

            if ($matches["ephoto_dam"] === "") {
                return [];
            }

            $valid_matches = [];
            foreach ($matches as $delta => $match) {
                if ($this->isValidSettings($delta)) {
                    $valid_matches[$delta] = $match;
                }
            }

            $valid_matches['id_div'] = $img_node->getAttribute("id");



            /**
             * Get alignement : right , center or left
             */
            $align = "default";

                        /**
             * Get the caption of the media
             */
            $caption_div = $img_node->nextElementSibling;

            if ($caption_div && $caption_div->tagName == "figcaption") {
                $matches["caption"] = $caption_div->nodeValue;
            }


            /*
            if(isset($matches["caption"])){
                $node_media = $node->nextElementSibling;
            }
            else{
                $node_media = $node->nextElementSibling->firstElementChild;
            }*/

            /*
            if ($valid_matches['ephoto_dam'] == "image") {
                $node_media = $node->firstElementChild->nextElementSibling;
            } else {
            }*/


            $node_media = $img_node;

            $classes = explode(" ", $node_media->getAttribute("class"));

            foreach ($classes as $class) {
                if (str_starts_with($class, "align-")) {
                    $align = explode("align-", $class)[1];
                    $matches["align"] = $align;
                }
            }





            if ($align == "default" && isset($node_media->nextElementSibling)) {
                $classes = explode(" ", $node_media->nextElementSibling->getAttribute("class"));

                foreach ($classes as $class) {
                    if (str_starts_with($class, "align-")) {
                        $align = explode("align-", $class)[1];
                        $matches["align"] = $align;
                    }
                }

                if ($align == "default") {
                    $align = "center";
                }
            }





            /**
             * Get the custom width of the media
             */

            if (isset($node_media) && $node_media->getAttribute("width")) {
                $matches["width"] = $node_media->getAttribute("width");
            }


            if (isset($matches["width"]) && $matches["width"]) {
                $valid_matches["width"] = $matches["width"];
            }

            if (isset($matches["caption"]) && $matches["caption"] != NULL) {
                $valid_matches["caption"] = mb_convert_encoding($matches["caption"],"UTF-8", "ISO-8859-1");
            }

            if (isset($matches["align"]) && $matches["align"] != NULL) {
                $valid_matches["align"] = $matches["align"];
            } else {
                $valid_matches["align"] = "center";
            }



            if ($valid_matches['ephoto_dam'] != "image") {


                if (isset($json->title)) {
                    $valid_matches['title'] = mb_convert_encoding($json->title,"UTF-8", "ISO-8859-1");
                }

                $url_components = parse_url($valid_matches["embed"]);
                parse_str($url_components['query'], $params);

                if (isset($params['height'])) {
                    $valid_matches['oldheight'] = $params['height'];
                }

                if (isset($params['width'])) {
                    $valid_matches['oldwidth'] = $params['width'];
                }


                if (isset($valid_matches["width"]) && $valid_matches["width"] != NULL && isset($valid_matches["oldwidth"])) {
                    $valid_matches["newHeight"] = intval(($valid_matches["width"] * $valid_matches["oldheight"]) / $valid_matches["oldwidth"]);
                }

                $valid_matches['embed'] = str_replace("width=" . $params['width'], "width=" . $valid_matches['width'], $valid_matches["embed"]);

                if (isset($valid_matches['newHeight'])) {
                    $valid_matches["embed"] = str_replace("&height=" . $params['height'], "&height=" . $valid_matches['newHeight'], $valid_matches["embed"]);
                }
            }


            $nodeString = $node->ownerDocument->saveXML($node);
            $globalDivToReturn = $nodeString;

            $divMedia = $globalDivToReturn;
            $return_valid_matches[$divMedia] = $valid_matches;


            $i++;
        }


        return $return_valid_matches;
    }

    /**
     * Check if the given settings are valid.
     *
     * @param mixed $settings
     *   Settings to validate.
     *
     * @return bool
     *   If the required settings are present.
     */
    protected function isValidSettings($settings)
    {
        return in_array($settings, self::DEFAULT_SETTINGS);
    }

    /**
     * Replace a string with a special characters free one.
     *
     * @param string $name
     *   The string to transform.
     *
     * @return string
     *   The transformed string
     */
    private function replace($name)
    {

        $name = str_replace("é", "e", $name);
        $name = str_replace("è", "e", $name);
        $name = str_replace("ê", "e", $name);
        $name = str_replace("à", "a", $name);
        $name = str_replace("â", "a", $name);
        $name = str_replace("î", "i", $name);
        $name = str_replace("ô", "o", $name);
        $name = str_replace("û", "u", $name);

        $name = str_replace("É", "E", $name);
        $name = str_replace("È", "E", $name);
        $name = str_replace("Ê", "E", $name);
        $name = str_replace("À", "A", $name);
        $name = str_replace("Â", "A", $name);
        $name = str_replace("Î", "I", $name);
        $name = str_replace("Ô", "O", $name);
        $name = str_replace("Û", "U", $name);
        return $name;
    }
}
