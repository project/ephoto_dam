![Ephoto Dam - Drupal Connector](drupal_connector.png)
# Ephoto Dam - Drupal Connector

## Description
Easily find your medias on the solution of digital asset management Ephoto Dam and import 
directly into Drupal 8, 9 or 10.

More information are available at our [project website](https://www.drupal.org/project/ephoto_dam).

More information are available at our [official website](https://ephoto.fr/). 

## Requirements
No special requirements, except CKEditor which is already installed by default.
Ephoto Dam Addon acts as its add-on.

| Drupal Connector | Drupal |
| :--------------: | :----: |
|       v1.*       |  v7.*  |
|       v2.*       |  v8.*  |
|       v3.*       |  v9.*  |
|       v4.*       |  v10.* |


Checked on Drupal 7.3.7

Checked on Drupal 8.7.8

Checked on Drupal 9.3.8

Checked on Drupal 10.1.3

## Configuration
Module comes already preconfigured to be used after installation.
If you want to change any options, do it as usual for text format 
you need; go to:

  Configuration > Content authoring > Text formats

choose and open text format you want to configure, then use CKEditor
and Ephoto Dam configuration widgets
to change preferences. Then press "Save".

## Development Environment

First we need to build docker image (see Dockerfile):

```bash
docker build -t ephoto-drupal .
```

Then, we start the environment with the docker-compose.yml file :

```bash
docker compose up -d
```

The application is now available here : http://localhost:8080
