/**
 * Ephoto Dam Addon for Drupal 9
 *
 * @base EPHOTO DAM FIELD
 * @author EINDEN
 */


(function () {

    Drupal.ephotoDamField = function (name, settings) {
        /**
         * Ephoto Dam ID of Drupal Addon
         */
        this.CLIENT_ID = 'CaCvqS5H';

        /**
         * Name of field
         */
        this._name = name;

        /**
         * Settings
         */
        this._settings = settings;

        /**
         * Current authid
         */
        this._authid = null;

        /**
         * Ephoto Dam instance
         */
        this._ephoto = null;

        /**
         * Ephoto Dam Server connected
         */
        this._connected = false;

        /**
         * Current selected field
         */
        this._fieldSelected = null;
    };

    Drupal.ephotoDamField.prototype = {
        /**
         * Enable version support (from default values form)
         *
         * @param {node} el
         */
        enableVersionSupport: function (el) {

            if (el.checked) {

                var elements = document.querySelectorAll('.ephoto-dam-field-version-field');
                elements.forEach(function(element) {
                    element.classList.remove('ephoto-dam-field-hidden');
                });

                var elements = document.querySelectorAll('.ephoto-dam-field-version-prefix');
                elements.forEach(function(element) {
                    element.classList.remove('ephoto-dam-field-hidden');
                });

                var elements = document.querySelectorAll('.ephoto-dam-field-versions-button');
                elements.forEach(function(element) {
                    element.classList.remove('ephoto-dam-field-hidden');
                });

                var elements = document.querySelectorAll('.ephoto-dam-field-file-button');
                elements.forEach(function(element) {
                    element.classList.add('ephoto-dam-field-hidden');
                });

            }

            else {


                var elements = document.querySelectorAll('.ephoto-dam-field-version-field');
                elements.forEach(function(element) {
                    element.classList.add('ephoto-dam-field-hidden');
                });

                var elements = document.querySelectorAll('.ephoto-dam-field-version-prefix');
                elements.forEach(function(element) {
                    element.classList.add('ephoto-dam-field-hidden');
                });

                var elements = document.querySelectorAll('.ephoto-dam-field-versions-button');
                elements.forEach(function(element) {
                    element.classList.add('ephoto-dam-field-hidden');
                });

                var elements = document.querySelectorAll('.ephoto-dam-field-file-button');
                elements.forEach(function(element) {
                    element.classList.remove('ephoto-dam-field-hidden');
                });

            }
        },

        loadScript: function(url, callback) {
            var script = document.createElement('script');
            script.src = url;
            script.onload = callback;
            script.onerror = function() {
                console.error('Failed to load script: ' + url);
            };
            document.head.appendChild(script);
        },

        /**
         * Load Ephoto Dam Api
         */
        load: function () {
            if (typeof ePhoto !== 'undefined') {
                this.init();
            }

            else if (this._settings && this._settings.server_url) {
                this.loadScript(this._settings.server_url + '/api/apiJS.js', this.init.bind(this))
            }
        },

        /**
         * Init Ephoto Dam Api
         */
        init: function () {
            var self = this;

            if (!ePhoto) {
                console.log('Error : loading the Ephoto Dam API');
                return;
            }

            //this._authid = Cookies.get('drupal_ephoto_dam_' + self._name + '_authid');

            this._ephoto = new ePhoto({
                'server': this._settings.server_url,
                'authID': this._authid,
                'client': this.CLIENT_ID
            });

            
            this._ephoto.connect();

            this._ephoto.File.enableDCore('json');

            this._ephoto.callOnConnect(() => {
                let version = self._ephoto.getVersion().split('.');

                // Requires API version 5.15+ (Ephoto Dam >4.0)
                if (version.length !== 3 || (version[0] < 6 && version[1] < 15)) {
                    alert(Drupal.t('The version of Ephoto Dam is not compatible with this feature') + ' (' + version + ')');
                    return;
                }

                self._authid = self._ephoto.getAuthID();

                self._connected = true;
            });
        },

        /**
         * Select a file versions
         */
        selectVersions: function () {
            if (!this._getFields()) {
                console.log('Error : field selection error');
                return;
            }

            this._ephoto.File.callOnFileReceived(this.getVersionsUrls.bind(this));

            this._ephoto.File.setButtons(this._ephoto.IMAGE_FILES, [{}]);

            this._ephoto.File.get({mode:'select'});
        },

        /**
         * Get URL of selected file versions
         *
         * @param {null} empty None value
         * @param {array} dcore DublinCore values
         */
        getVersionsUrls: function (empty, dcore) {
            if (!dcore || dcore === 'not logged in') {
                return;
            }

            let identifier = parseInt(dcore['dc:identifier:number'], 10);
            let thumbnail = dcore['dc:description.thumbnail'];

            if (!thumbnail) {
                thumbnail = dcore['dc:relation'];
            }

            if (!identifier || !thumbnail) {
                console.log('Notice : dublincore values missing');
                return;
            }

            const caption = this._buildCaption(dcore);

            this._ephoto.File.getVersions(identifier,async function (versions) {
                const fields = this._getFields();

                
                let missing_versions = 0;

                for (const inc in fields) {

                    if (!fields[inc].name.length || typeof versions[fields[inc].name] === 'undefined') {
                        fields[inc].input.value = '';
                        fields[inc].identifier.value = '';
                        fields[inc].caption.value = '';
                        fields[inc].thumbnail.value = '';

                        fields[inc].thumbnail.previousElementSibling.setAttribute('src', '');

                        missing_versions++;
                        continue;
                    }

                    let size = this._checkSizeValue(fields[inc].size);
                    let version = fields[inc].name;
                    let file = version + '@' + identifier;

                    let button = this._ephoto.File.setButtons( this._ephoto.IMAGE_FILES, [ {
                        'mode': 'link',
                        'definition': 'custom',
                        'size': size

                    } ] ).get(0);

	

                    await button.getLink([file], function (url) {

                        fields[inc].input.value = url;

                        if(fields[inc].identifier){
                            fields[inc].identifier.value = identifier;
                        }else{
                            fields[inc].identifier = identifier;
                        }


                        fields[inc].caption.value = caption;
                        fields[inc].thumbnail.value = thumbnail;

                        fields[inc].thumbnail.previousElementSibling.setAttribute('src', thumbnail);
                    })
					
					// necessaire en attendant de passer getLink en async
					await new Promise(resolve => setTimeout(resolve, 0));

					
                }

                if (missing_versions === 1) {
                  alert(Drupal.t('A version is missing for the selected file'));
                }

                else if (missing_versions) {
                  alert(Drupal.t('Multiple versions are missing for the selected file'));
                }

            }.bind(this));
        },

        /**
         * Select a file
         *
         * @param {number} delta Field index
         */
        selectFile: function (delta) {
            const fields = this._getFields();

            if (!fields.length) {
                console.log('Error : field selection error (1)');
                return;
            }

            this._fieldSelected = fields[delta];

            if (!this._fieldSelected) {
                console.log('Error : field selection error (2)');
                return;
            }

            let size = this._checkSizeValue(this._fieldSelected.size);

            this._ephoto.File.callOnFileReceived(this.getFileUrl.bind(this));

            this._ephoto.File.setButtons( this._ephoto.IMAGE_FILES, [ {
                'mode': 'link',
                'definition': 'custom',
                'size': size
            } ] );

            // In development
            /*
            this._ephoto.File.setButtons( this._ephoto.VIDEO_FILES, [ {
                'mode': 'embed',
                'definition': 'custom',
                'size': size
            } ] );
            */

            this._ephoto.File.get();
        },

        /**
         * Get URL of selected file
         *
         * @param {string} url URL value
         * @param {object} dcore DublinCore values
         */
        getFileUrl: function (url, dcore) {
            if (url === 'fileDoesNotExist') {
                console.log('Notice : file does not exist');
                return;
            }

            if (!dcore) {
                return;
            }

            if (!this._fieldSelected) {
                console.log('Notice : field selected missing');
                return;
            }

            let identifier = parseInt(dcore['dc:identifier:number'], 10);
            let thumbnail = dcore['dc:description.thumbnail'];

            if (!thumbnail) {
                thumbnail = dcore['dc:relation'];
            }

            if (!identifier || !thumbnail) {
                console.log('Notice : dublincore values missing');
                return;
            }

            const caption = this._buildCaption(dcore);

            this._fieldSelected.input.value = url;
            if(this._fieldSelected.identifier){
                this._fieldSelected.identifier.value = identifier;
            }else{
                this._fieldSelected.identifier = identifier;
            }
            this._fieldSelected.caption.value = caption;
            this._fieldSelected.thumbnail.value =thumbnail;

            this._fieldSelected.thumbnail.previousElementSibling.setAttribute('src', thumbnail);

        },

        /**
         * Check size value
         *
         * @param {number} size A size value
         */
        _checkSizeValue: function (size) {
            return size.toString().split('x').map((val) => {
                val = val === '' || isNaN(val) ? 0 : parseInt(val, 10);
                return (val > 4000) ? 4000 : ((val <= 0) ? '' : val);

            }).join('x');
        },

        /**
         * Get all fields
         */
        _getFields: function () {
            let fieldname = this._name.replace(/_/g, '-');
            let bloc = document.getElementById(fieldname + '-values');
            if (!bloc || !bloc.length) {
                bloc = document.getElementById('edit-' + fieldname + '-wrapper');
            }

            let fields = [];
            let sizes = Array.from(bloc.querySelectorAll('.ephoto-dam-field-imagesize-field'));
            let inputs = Array.from(bloc.querySelectorAll('.ephoto-dam-field-url-field'));
            let identifiers = Array.from(bloc.querySelectorAll('.ephoto-dam-field-identifier-field'));
            let captions = Array.from(bloc.querySelectorAll('.ephoto-dam-field-caption-field'));
            let thumbnails = Array.from(bloc.querySelectorAll('.ephoto-dam-field-thumbnail'));




            Array.from(bloc.querySelectorAll('.ephoto-dam-field-version-field')).map((index, el) => {

                let size,input,identifier,caption,thumbnail = "";

                if(sizes[el]){
                    size =  sizes[el]
                }

                
                if(inputs[el]){
                    input=inputs[el];
                }

                if(identifiers[el]){
                    identifier=identifiers[el];
                }

                if(captions[el]){
                    caption = captions[el];
                }

                if(thumbnails[el]){
                    thumbnail = thumbnails[el]
                }

                fields.push({
                    'name' : index.value,
                    'size' : size,
                    'input' : input,
                    'identifier' : identifier,
                    'caption' : caption,
                    'thumbnail' : thumbnail
                });
            });



            return fields;
        },

        /**
         * Build the caption width a DublinCore
         *
         * @param {array} dcore Dublin Core values
         */
        _buildCaption: function (dcore) {
            let result = this._settings.captions_format;

            for (const property in dcore) {
                result = result.replace('[' + property + ']', dcore[property]);
            }

            return result;
        }
    }

    EphotoDamField = {};

    for (const name in drupalSettings.ephotoDamField) {
        EphotoDamField[name] = new Drupal.ephotoDamField(name, drupalSettings.ephotoDamField[name]);
        EphotoDamField[name].load();
    }

    })();
