<?php

namespace Drupal\ephoto_dam_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Ephoto Dam Widget.
 *
 * @FieldWidget(
 *   id = "ephoto_dam_field_widget",
 *   label = @Translation("Ephoto Dam Field"),
 *   field_types = {
 *     "ephoto_dam_field"
 *   },
 * )
 */
class EphotoDamFieldWidget extends WidgetBase {
  /**
   * The attribute of the class.
   *
   * @var array
   */
  private static $first = [];

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $is_default_values_form = $this->isDefaultValueWidget($form_state);
    $version_support = $this->getFieldSetting('version_support');
    $field_name = $this->fieldDefinition->getName();

    if (empty(self::$first[$field_name])) {
      $classes = [
        'button',
        'ephoto-dam-field-button',
        'ephoto-dam-field-versions-button',
      ];
      if (!$version_support) {
        $classes[] = 'ephoto-dam-field-hidden';
      }

      $element['versions-select'] = [
        '#type' => 'button',
        '#value' => t('Select'),
        '#disabled' => $is_default_values_form,
        '#attributes' => [
          'class' => $classes,
          'onclick' => 'EphotoDamField[\'' . $field_name . '\'].selectVersions();return false',
        ],
      ];

      if (!$is_default_values_form && $version_support) {
        $thumbnail = !empty($items[$delta]->thumbnail) ? $items[$delta]->thumbnail : NULL;

        $identifier = !empty($items[$delta]->identifier) ? $items[$delta]->identifier : NULL;

        $element['thumbnail'] = [
          '#type' => 'hidden',
          '#maxlength' => 256,
          '#default_value' => $thumbnail,
          '#prefix' => '<img class="ephoto-dam-field-thumbnail-image ephoto-dam-field-thumbnail-version"' . ($thumbnail ? ' src="' . $thumbnail . '"' : '') . '/>',
          '#attributes' => [
            'class' => ['ephoto-dam-field-thumbnail'],
          ],
        ];

        $element['caption'] = [
          '#type' => 'textarea',
          '#title' => t('Caption'),
          '#default_value' => isset($items[$delta]->caption) ? $items[$delta]->caption : NULL,
          '#rows' => 1,
          '#maxlength' => 256,
          '#attributes' => [
            'class' => ['ephoto-dam-field-caption-field'],
            'readonly' => 'readonly',
            'onclick' => 'this.focus();this.select()',
          ],
        ];

        $element['identifier'] = [
          '#type' => 'number',
          '#title' => t('File identifier'),
          '#maxlength' => 20,
          '#default_value' => $identifier,
          '#size' => 10,
          '#attributes' => [
            'class' => ['ephoto-dam-field-identifier-field'],
            'readonly' => 'readonly',
            'onclick' => 'this.focus();this.select()',
          ],
        ];
      }
    }

    $classes = ['ephoto-dam-field-field', 'ephoto-dam-field-url-field'];
    if (!$is_default_values_form) {
      $classes[] = 'ephoto-dam-field-hidden';
    }

    $suffix_classes = ['ephoto-dam-field-url-suffix'];

    $element['url'] = [
      '#type' => 'textfield',
      '#title' => t('URL') . ' ' . ($delta + 1),
      '#default_value' => isset($items[$delta]->url) ? $items[$delta]->url : NULL,
      '#size' => 70,
      '#maxlength' => 256,
      '#field_suffix' => '<span class="' . implode(' ', $suffix_classes) . '"></span>',
      '#attributes' => [
        'class' => ['ephoto-dam-field-field', 'ephoto-dam-field-url-field'],
        'readonly' => 'readonly',
        'onclick' => 'this.focus();this.select()',
      ],
    ];

    $classes = ['ephoto-dam-field-field', 'ephoto-dam-field-imagesize-field'];
    $suffix_classes = ['ephoto-dam-field-imagesize-suffix'];

    if (!$is_default_values_form && empty($items[$delta]->image_size)) {
      $classes[] = 'ephoto-dam-field-hidden';
      $suffix_classes[] = 'ephoto-dam-field-hidden';
    }

    $element['image_size'] = [
      '#type' => 'textfield',
      '#title' => '&nbsp;',
      '#default_value' => isset($items[$delta]->image_size) ? $items[$delta]->image_size : NULL,
      '#size' => 4,
      '#maxlength' => 60,
      '#field_suffix' => '<span class="' . implode(' ', $suffix_classes) . '">px</span>',
      '#attributes' => [
        'class' => $classes,
        'placeholder' => t('Size'),
        'max' => 1600,
        'onclick' => 'this.focus();this.select()',
      ],
    ];

    $classes = ['ephoto-dam-field-field', 'ephoto-dam-field-version-field'];
    $prefix_classes = ['ephoto-dam-field-version-prefix'];

    if (!$version_support || (!$is_default_values_form && empty($items[$delta]->version))) {
      $classes[] = 'ephoto-dam-field-hidden';
      $prefix_classes[] = 'ephoto-dam-field-hidden';
    }

    $element['version'] = [
      '#type' => 'textfield',
      '#title' => '&nbsp;',
      '#default_value' => !empty($items[$delta]->version) ? $items[$delta]->version : NULL,
      '#size' => 10,
      '#maxlength' => 60,
      '#field_prefix' => '<span class="' . implode(' ', $prefix_classes) . '">, &nbsp;' . t('version') . '</span>',
      '#attributes' => [
        'class' => $classes,
        'placeholder' => t('Name'),
        'onclick' => 'this.focus();this.select()',
      ],
    ];

    $classes = [
      'button',
      'ephoto-dam-field-button',
      'ephoto-dam-field-file-button',
    ];
    if ($version_support) {
      $classes[] = 'ephoto-dam-field-hidden';
    }

    $element['file-select'] = [
      '#type' => 'button',
      '#value' => t('Select'),
      '#disabled' => $is_default_values_form,
      '#attributes' => [
        'class' => $classes,
        'onclick' => 'EphotoDamField[\'' . $field_name . '\'].selectFile(' . $delta . ');return false',
      ],
    ];

    // self::$first[$field_name].
    if (!$is_default_values_form && (!$version_support || !empty(self::$first[$field_name]))) {
      $prefix_classes = ['ephoto-dam-field-thumbnail-image'];
      if ($version_support) {
        $prefix_classes[] = 'ephoto-dam-field-hidden';
      }

      $thumbnail = !empty($items[$delta]->thumbnail) ? $items[$delta]->thumbnail : NULL;

      $element['thumbnail'] = [
        '#type' => 'hidden',
        '#maxlength' => 256,
        '#default_value' => $thumbnail,
        '#prefix' => '<img class="' . implode(' ', $prefix_classes) . '"' . ($thumbnail ? ' src="' . $thumbnail . '"' : '') . '/>',
        '#attributes' => [
          'class' => ['ephoto-dam-field-thumbnail'],
        ],
      ];

      $element['caption'] = [
        '#type' => $version_support ? 'hidden' : 'textarea',
        '#title' => t('Caption') . ' ' . ($delta + 1),
        '#default_value' => isset($items[$delta]->caption) ? $items[$delta]->caption : NULL,
        '#rows' => 1,
        '#maxlength' => 256,
        '#attributes' => [
          'class' => ['ephoto-dam-field-caption-field'],
          'readonly' => 'readonly',
          'onclick' => 'this.focus();this.select()',
        ],
      ];
    }

    if (!$is_default_values_form) {
      // It should be possible to change the value
      // (eg. in the case of unlimited values)
      $element['image_size']['#attributes']['readonly'] = 'readonly';
      $element['version']['#attributes']['readonly'] = 'readonly';
    }

    self::$first[$field_name] = TRUE;

    return $element;
  }

}
