<?php

namespace Drupal\ephoto_dam_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TraversableTypedDataInterface;

/**
 * Plugin implementation of the Ephoto Dam field type.
 *
 * @FieldType(
 *   id = "ephoto_dam_field",
 *   label = @Translation("Ephoto Dam Field"),
 *   description = @Translation("Stores a Ephoto Dam URL and then outputs some embed code"),
 *   category = @Translation("Media"),
 *   default_widget = "ephoto_dam_field_widget",
 *   default_formatter = "ephoto_dam_field_formatter",
 *   constraints = {"EphotoDamFieldValidation" = {}}
 * )
 */
class EphotoDamField extends FieldItemBase {

  /**
   * Default settings.
   */
  private const DEFAULT_SETTINGS = [
    'version_support' => FALSE,
    'captions_format' => '',
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    return new static($definition, $name, $parent);
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'identifier' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'url' => [
          'type' => 'varchar',
          'length' => 256,
        ],
        'image_size' => [
          'type' => 'varchar',
          'length' => 9,
        ],
        'version' => [
          'type' => 'varchar',
          'length' => 20,
        ],
        'caption' => [
          'type' => 'varchar',
          'length' => 256,
        ],
        'thumbnail' => [
          'type' => 'varchar',
          'length' => 256,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['identifier'] = DataDefinition::create('integer')
      ->setLabel(t('File identifier'));

    $properties['url'] = DataDefinition::create('string')
      ->setLabel(t('Url'));

    $properties['image_size'] = DataDefinition::create('string')
      ->setLabel(t('Image size'));

    $properties['version'] = DataDefinition::create('string')
      ->setLabel(t('Version'));

    $properties['caption'] = DataDefinition::create('string')
      ->setLabel(t('Caption'));

    $properties['thumbnail'] = DataDefinition::create('string')
      ->setLabel(t('Thumbnail'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $url = $this->get('url')->getValue();
    $image_size = $this->get('image_size')->getValue();
    $version = $this->get('version')->getValue();

    return empty($url) && empty($image_size) && empty($version);
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $field_name = $this->getParent()->getName();

    $form = [];

    $form['version_support'] = [
      '#title' => t('Version support'),
      '#description' => t('An url will be automatically associated with each version with the same label in Ephoto Dam') . '.',
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('version_support'),
      '#attributes' => [
        'class' => ['ephoto-dam-field-settings-version-support'],
        'onclick' => 'EphotoDamField[\'' . $field_name . '\'].enableVersionSupport(this)',
      ],
    ];

    $form['captions_format'] = [
      '#title' => t('Captions format'),
      '#type' => 'textarea',
      '#default_value' => $this->getSetting('captions_format'),
      '#description' => t('Please format the captions'),
      '#rows' => 1,
    ];

    return $form;
  }

  /**
   * Gets the preview HTML.
   */
  public function getHtmlPreview($first) {
    $html = '';

    $url = $this->get('url')->getValue();

    if ($url) {
      $thumbnail = $this->get('thumbnail')->getValue();
      $identifier = $this->get('identifier')->getValue();
      $image_size = $this->get('image_size')->getValue();
      $version = $this->get('version')->getValue();
      $caption = $this->get('caption')->getValue();

      $html = '<div class="ephoto-dam-field-preview-container">';
      if ($first) {
        $html .= '<img class="ephoto-dam-field-preview-thumbnail" src="' . $thumbnail . '">';
      }

      else {
        $html .= '<div class="ephoto-dam-field-preview-thumbnail"></div>';
      }

      $html .= '<div class="ephoto-dam-field-preview-content">';

      if ($caption && $first) {
        $html .= '<div class="ephoto-dam-field-preview-caption">' . t('Caption') . ' : ' . $caption . '</div>';
      }

      $html .= '<div class="ephoto-dam-field-preview-url"><a href="' . $url . '" target="_blank">' . $url . '</a></div>';

      if($image_size){
        $html .= '<div class="ephoto-dam-field-preview-bubble">' . t('Size') . ' : ' . $image_size . 'px</div>';
      }

      if ($version) {
        $html .= '<div class="ephoto-dam-field-preview-bubble">' . t('Name') . ' : ' . $version . '</div>';
      }

      if ($identifier) {
        $html .= '<div class="ephoto-dam-field-preview-bubble">' . t('File identifier') . ' : ' . $identifier . '</div>';
      }

      $html .= '</div>';
      $html .= '</div>';
    }

    return $html;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return self::DEFAULT_SETTINGS;
  }

}
