<?php

namespace Drupal\ephoto_dam_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Contrainet.
 *
 * @Constraint(
 *   id = "EphotoDamFieldValidation"
 * )
 * @category Fiel_Constraint
 */
class EphotoDamFieldConstraint extends Constraint {

}
