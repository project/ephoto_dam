<?php

namespace Drupal\ephoto_dam_field\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Ephoto Dam Field.
 */
class EphotoDamFieldConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function validate($field, Constraint $constraint) {
    if (!isset($field->image_size)) {
      return NULL;
    }

    $test = empty($field->image_size) || preg_match('/^[0-9x]+$/', $field->image_size);
    $message = t('The "Size" field must respect one of the following patterns: "x{height}", "{width}x", "{width}x{height}" or "{width/height (detect auto)}". Examples: "x500", "500x", "500x500" or "500".');

    if (!$test) {
      $this->context->addViolation($message);
    }
  }

}
