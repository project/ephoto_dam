import { Plugin } from 'ckeditor5/src/core';
import { Widget } from 'ckeditor5/src/widget';
import InsertEphotoDamCommand from './ephotodampress';

// cSpell:ignore InsertEphotoDamCommand

/**
 * CKEditor 5 plugins do not work directly with the DOM. They are defined as
 * plugin-specific data models that are then converted to markup that
 * is inserted in the DOM.
 *
 * This file has the logic for defining the model, and for how it is
 * converted to standard DOM markup.
 */
export default class EphotoDamAdd extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this.editor.commands.add(
      'insertEphotoDam',
      new InsertEphotoDamCommand(this.editor),
    );
  }

  /*
   * This registers the structure that will be seen by CKEditor 5 as

   *
   * The logic in _defineConverters() will determine how this is converted to
   * markup.
   */
  _defineSchema() {

    // Schemas are registered via the central `editor` object.
    const schema = this.editor.model.schema;

    schema.register('simpleBox', {
      // Behaves like a self-contained object (e.g. an image).
      isObject: true,
      // Allow in places where other blocks are allowed (e.g. directly in the root).
      allowWhere: '$block',
    });

    schema.register('ephotoDamTitle', {
      // This creates a boundary for external actions such as clicking and
      // and keypress. For example, when the cursor is inside this box, the
      // keyboard shortcut for "select all" will be limited to the contents of
      // the box.
      isLimit: true,
      // This is only to be used within simpleBox.
      allowIn: 'simpleBox',
      // Allow content that is allowed in blocks (e.g. text with attributes).
      allowContentOf: '$block',
    });

    schema.register('ephotoDamDescription', {
      isLimit: true,
      allowIn: 'simpleBox',
      allowContentOf: '$root',
    });

    schema.addChildCheck((context, childDefinition) => {
      // Disallow inside ephotoDamDescription.
      if (
        context.endsWith('ephotoDamDescription') &&
        childDefinition.name === 'simpleBox'
      ) {
        return false;
      }
    });
  }

}
