/**
 * @file registers the toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../icons/ephotoDamIcon.svg';

var url;
var processed = false;

export default class EphotoDamUI extends Plugin {

  /**
   * Create the Ephoto Dam button and iteractions
   */
  init() {
    var self = this;

    (function (Drupal, drupalSettings) {

      Drupal.behaviors.EphotoDamModuleBehavior = {
        attach: function (context, settings) {

          // Make sure not to handle several times the same event
          if (!processed) {

            url = drupalSettings.ephoto_dam;

            // Get the div of the editor
            var editorElement = document.querySelector('.ck-editor__editable');

            if (editorElement.ckeditorInstance) {
              processed = true;
              var node_insert = null;
              var id_insert = "";

              const editor = editorElement.ckeditorInstance;
              const config = editor.config.get('ephoto_dam');
              const root = editor.model.document.getRoot();

              // Create the style for the size of the different EphotoDam element type at the importaiton
              var style = document.createElement('style');
              style.innerHTML = '.ck-content figure:has(.ckeditor_ephoto_dam)  { width: '+config.documents_size+'px; };';
              document.getElementsByTagName('head')[0].appendChild(style);

              style = document.createElement('style');
              style.innerHTML = '.ck-content figure:has(.img_ephoto_dam ) { width: '+config.images_size+'px; };';
              document.getElementsByTagName('head')[0].appendChild(style);

              style = document.createElement('style');
              style.innerHTML = '.ck-content figure:has(.MovingImage_ephoto_dam) { width: '+config.videos_size+'px; };';
              document.getElementsByTagName('head')[0].appendChild(style);

              // Add this style to the page
              document.getElementsByTagName('head')[0].appendChild(style);

              editor.model.document.on('change:data', async () => {

                const model = editor.model;
                var selectedElement = null;
                var remove = null;
                var id_media = -1;

                // Check if any caption elements were modified
                const changes = model.document.differ.getChanges();

                // For each modification
                changes.forEach(change => {
                  // Check if the modification is removing captions
                  if ((change.type === 'remove' && change.name === 'caption')) {
                    remove = true;
                  }

                  // Check if the modification is inserting an image
                  if ((change.type === 'insert' && change.name === 'imageBlock')) {
                    const position = change.position;
                    let node = root.getChild(position.path[0]);
                    node_insert = node;
                  }
                });
                // Set the element who was changed
                selectedElement = node_insert;

                // If the change was the insertion of an image
                if (selectedElement && selectedElement.name === 'imageBlock' && !remove && config.captions) {

                  if (!selectedElement.childCount) {

                    // Get the id of the image
                    const allattributes = selectedElement.getAttribute('htmlImgAttributes');
                    if (allattributes && allattributes.attributes) {
                      id_insert = allattributes.attributes.id;

                      var id_media = self.getValueFromData(allattributes.attributes.data, "id_image");
                    }
                    else {
                      id_insert = "";
                    }
                    
                    // If it's the id of an EphotoDam media
                    if (id_insert.startsWith("media_ephoto_dam_")) {

                      var auth_Id = self.getCookie("ephoto_dam_authid");

                      // Fill the media caption with the infos of the media from Ephoto Sam
                      var retour = await self.replaceCaptionMedia(id_media, auth_Id, config.captions_format);

                      // Edit the media in the editor
                      model.change(writer => {

                        // Add the filled caption to the media
                        const caption = writer.createElement('caption');
                        writer.append(caption, selectedElement);
                        writer.insertText(retour, caption);
                        writer.setSelection(caption, 'in');
                      });

                    }

                  }
                  // Unset the inserted element
                  node_insert = null;
                }


              });
            }

          }
        }
      }
    })(Drupal, drupalSettings);



    const editor = this.editor;

    // Allow <iframe> elements in the model.
    editor.model.schema.register('iframe', {
      allowWhere: '$text',
      allowContentOf: '$block'
    });
    // Allow <iframe> elements in the model to have all attributes.
    editor.model.schema.addAttributeCheck(context => {
      if (context.endsWith('iframe')) {
        return true;
      }
    });
    // View-to-model converter converting a view <iframe> with all its attributes to the model.
    editor.conversion.for('upcast').elementToElement({
      view: 'iframe',
      model: (viewElement, modelWriter) => {
        return modelWriter.writer.createElement('iframe', viewElement.getAttributes());
      }
    });

    // Model-to-view converter for the <iframe> element (attributes are converted separately).
    editor.conversion.for('downcast').elementToElement({
      model: 'iframe',
      view: 'iframe'
    });









    // Model-to-view converter for <iframe> attributes.
    // Note that a lower-level, event-based API is used here.
    editor.conversion.for('downcast').add(dispatcher => {
      dispatcher.on('attribute', (evt, data, conversionApi) => {
        // Convert <iframe> attributes only.

        if (data.item.name == 'iframe') {
          const viewWriter = conversionApi.writer;
          const viewIframe = conversionApi.mapper.toViewElement(data.item);

          // In the model-to-view conversion we convert changes.
          // An attribute can be added or removed or changed.
          // The below code handles all 3 cases.
          if (data.attributeNewValue) {
            viewWriter.setAttribute(data.attributeKey, data.attributeNewValue, viewIframe);
          } else {
            viewWriter.removeAttribute(data.attributeKey, viewIframe);
          }
        }

        if (data.attributeNewValue == null || data.attributeNewValue.classes == null || !data.attributeNewValue.classes.includes("ckeditor_ephoto_dam")) {
          return;
        }
        const id = data.attributeNewValue.attributes.id;

        // Add the interation to activate the zoom with a right click
        window.requestAnimationFrame(() => {
          this.addImageZoomConfig(id);
        });
        
        const viewWriter = conversionApi.writer;
        const viewIframe = conversionApi.mapper.toViewElement(data.item);

        // In the model-to-view conversion we convert changes.
        // An attribute can be added or removed or changed.
        // The below code handles all 3 cases.
        if (data.attributeNewValue) {
          viewWriter.setAttribute(data.attributeKey, data.attributeNewValue, viewIframe);
        } else {
          viewWriter.removeAttribute(data.attributeKey, viewIframe);
        }
      });
    });

    // This will register the toolbar button.
    editor.ui.componentFactory.add('simpleBox', (locale) => {
      const command = editor.commands.get('insertEphotoDam');
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('EphotoDam'),
        icon,
        tooltip: false,
        class: "ephoto_dam_tooltip_icon"
      });

      // Bind the state of the button to the command.
      buttonView.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () =>
        editor.execute('insertEphotoDam'),
      );

      return buttonView;
    });
  }



  /**
   * 
   * @param {string} id_media 
   * @param {string} auth_Id 
   * @param {string} currentLegend 
   * @returns 
   */
  async replaceCaptionMedia(id_media, auth_Id, currentLegend) {
    // Check for text in brackets
    const matches = currentLegend.match(/\[(.*?)\]/g);
    var newValue = currentLegend;

    // If there is text in brackets
    if (matches) {
      var urlInfo = drupalSettings.ephoto_dam.server_url + 'api/' + auth_Id + '/file/getInfo?id=' + id_media;

      var parser = new DOMParser();
      // Get the infos for the innserted media
      var newValue = await fetch(urlInfo)
        .then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.text();

        })
        .then(data => {

          var xmlDoc = parser.parseFromString(data, "text/xml");

          var fields = xmlDoc.getElementById("Desc");

          // For each text in brackets
          matches.forEach(match => {

            fields.querySelectorAll('field').forEach(element => {
              var fieldName = element.querySelector('name');

              var matchText = match.slice(1, match.length - 1);
              // Check if there is a field in Ephoto Dam with the same name with a value
              if (fieldName.textContent && fieldName.textContent == matchText) {
                var replaceValue = element.querySelector('value').textContent;
                // If so, replace the text in brackets with the value
                newValue = newValue.replaceAll("["+matchText+"]",replaceValue)
              }

            });

          });

          return newValue;
        })
        .catch(error => {
          console.error("Erreur lors de la requête :", error);
          return newValue;

        });
    }

    return newValue;
  }


  /**
   * Adds the functionality to turn on or off the zoom on images
   * 
   * @param {string} id 
   */
  addImageZoomConfig(id) {

    // check if it's an image
    if (this.getDataCalc(id, "ephoto_dam") == "image") {

      var noContext = document.getElementById(id);

      noContext.addEventListener("contextmenu", (e) => {
        e.preventDefault();

        let menuCustomToDelete = document.getElementsByClassName("ephoto_dam_properties");
        while (menuCustomToDelete.length > 0) {
          menuCustomToDelete[0].parentNode.removeChild(menuCustomToDelete[0]);
        }

        var value = this.getDataCalc(id, "zoom")


        var contextMenu = '<div id="context-menu">';
        if (value == "true") {
          contextMenu += '<div class="item">' + Drupal.t('Disable zoom') + '</div>';
        } else {
          contextMenu += '<div class="item">' + Drupal.t('Enable zoom') + '</div>';
        }
        contextMenu += '</div>';

        var divAbove = document.createElement("div");
        divAbove.innerHTML = contextMenu;
        divAbove.id = "ephoto_dam_properties_" + id;
        divAbove.className = "ephoto_dam_properties";

        document.getElementsByTagName("body")[0].appendChild(divAbove);
        let menuCustom = document.getElementById("ephoto_dam_properties_" + id);
        let x = e.clientX, y = e.clientY;

        menuCustom.style.left = `${x}px`;
        menuCustom.style.top = `${y}px`;
        menuCustom.style.visibility = "visible";
        menuCustom.style.position = "fixed";
        menuCustom.style.zIndex = "2";

        menuCustom.addEventListener("click", (e) => {
          if (value == "true") {

            this.setDataZoom(id, "false");
          } else {
            this.setDataZoom(id, "true");
            this.editor.updateSourceElement();
          }
        });
      });

      document.addEventListener("wheel", (e) => {
        let menuCustom = document.getElementById("ephoto_dam_properties_" + id);
        if (menuCustom) {
          menuCustom.remove();
        }
      });

      document.addEventListener("click", (e) => {
        let menuCustom = document.getElementById("ephoto_dam_properties_" + id);
        if (menuCustom) {
          menuCustom.remove();
        }
      });
    }
  }

  /**
   * Update the value of the zoom of an Ephoto Dam image
   * 
   * @param {string} id 
   * @param {string} newData 
   */
  setDataZoom(id, newData) {
    var doc = this.dataToHtml();

    var tempdata = this.updateValueFromData(id, "zoom", newData);

    doc.getElementById(id).setAttribute("data", tempdata);

    var retour = doc.querySelector("body");

    this.editor.setData(retour.innerHTML);
  }

  /**
   * Get the editor to html format
   * 
   * @returns 
   */
  dataToHtml() {
    var tempdata = this.editor.getData();
    var parser = new DOMParser();
    var doc = parser.parseFromString(tempdata, 'text/html');
    return doc;
  }

  /**
   * Gets the value of an Ephoto Dam media
   * 
   * @param {string} id 
   * @param {string} dataToRetrieve 
   * @returns 
   */
  getDataCalc(id, dataToRetrieve) {
    var value = "";
    var media = document.getElementById(id);

    if (media) {
      var data = media.getAttribute("data");
      var value = this.getValueFromData(data, dataToRetrieve);
    }
    return value;
  }

  /**
   * Gets the value of an Ephoto Dam media json format string
   * 
   * @param {string} data 
   * @param {string} dataToRetrieve 
   * @returns 
   */
  getValueFromData(data, dataToRetrieve) {
    data = data.replaceAll('\'', '"');
    var json_value = JSON.parse(data);
    var value = json_value[dataToRetrieve];
    return value;
  }

  /**
   * Update the value of the json data of an Ephoto Dam media
   * 
   * @param {string} id 
   * @param {string} attribute 
   * @param {string} value 
   * @returns 
   */
  updateValueFromData(id, attribute, value) {

    var data = document.getElementById(id).getAttribute("data");
    data = data.replaceAll('\'', '"');

    var json_value = JSON.parse(data);
    json_value[attribute] = value;

    data = JSON.stringify(json_value);
    data = data.replaceAll('"', '\'');

    return data
  }

  /**
   * Get the value of a coockie parameter
   * 
   * @param {string} name 
   * @returns string
   */
  getCookie(name) {
    let cookie = {};
    document.cookie.split(';').forEach(function (el) {
      let split = el.split('=');
      cookie[split[0].trim()] = split.slice(1).join("=");
    })
    return cookie[name];
  }

}
