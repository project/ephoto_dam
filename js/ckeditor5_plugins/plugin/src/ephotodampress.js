/**
 * @file defines InsertEphotoDamCommand, which is executed when the
 * toolbar button is pressed.
 */

var script = document.createElement('script');
var url = drupalSettings.ephoto_dam.server_url;
script.type = 'text/javascript';
script.src = url + 'api/apiJS.js';
document.body.appendChild(script);

import { Command } from 'ckeditor5/src/core';


export default class InsertEphotoDamCommand extends Command {


  /**
   * This function calls the Ephoto Dam API
   *
   * It starts by pulling the API from the entered Ephoto Dam URL
   * It sets the buttons sizes which will be used
   * Finally it opens the Ephoto Dam into a popup window
   */
  execute() {

    const { model } = this.editor;

    var urlAuth = drupalSettings.ephoto_dam.server_url + 'api/auth/getNewAuthID';
    var conn = new XMLHttpRequest();
    conn.open("GET", urlAuth);
    conn.send();

    var self = this;
    conn.onreadystatechange = function () {
      if (conn.readyState === 4) {
        if (conn.status === 200) {
          var expiration = Date.now() + (60 * 30);
          document.cookie = "ephoto_dam_authid=" + conn.responseText + "; expires=" + expiration;
          drupalSettings.ephoto_dam.auth_id = conn.responseText;
          self.connectEphoto();
        }
      }
    };
  }

  /**
   * Open Ephoto Dam pop up
   * 
   * It starts by setting the parameters by type of media
   * It then opens up a pop up to chose the media to get
   */
  connectEphoto() {

    var ephoto = new EphotoDam({
      server: drupalSettings.ephoto_dam.server_url,
      authID: drupalSettings.ephoto_dam.auth_id,
      client: 'bD3ui8br'
    });

    ephoto.connect();
    ephoto.File.setMode('embed');
    const config = this.editor.config.get('ephoto_dam');

    ephoto.File.setButtons(ephoto.IMAGE_FILES, [{
      'definition': !config.zoom && config.images_size <= 320 ? 'low' : 'middle',
      'size': config.images_size
    }]);

    ephoto.File.setButtons(ephoto.MOVIE_FILES, [{
      'definition': 'custom',
      'size': config.videos_size
    }]);

    ephoto.File.setButtons(ephoto.DOCUMENT_FILES, [{
      'definition': 'custom',
      'size': config.documents_size
    }]);

    ephoto.File.setButtons(ephoto.MODEL_FILES, [{
      'definition': 'custom',
      'size': config.documents_size
    }]);

    ephoto.File.callOnFileReceived(this.insertFile.bind(this));

    ephoto.File.enableDCore();
    ephoto.File.get();
  }

  /**
   * Retrieve the imported media from the returned objects and add it to the page
   *
   * @param {string} html
   * @param {ephotoDamObject} dcore
   */
  insertFile(html, dcore) {

    //let tag;
    if (!html || html === 'not logged in' || !dcore) {
      return;
    }

    if (html === 'failure') { return; }
    if (html === 'fileDoesNotExist') { return; }

    let dcid = this.getValueOfXmlNode(dcore, 'dc:identifier');

    this.getlink(drupalSettings.ephoto_dam.auth_id, dcid, dcore, html);
  }




  /**
   * Prepare the media tag to be si
   * 
   * @param {string} link 
   * @param {*} dcore 
   * @param {*} html 
   */
  preparemedia(link, dcore, html) {

    const config = this.editor.config.get('ephoto_dam');
    const id = "ephoto_dam_" + this.genRandonString(10);
    let imageSrc, json, width, src;
    let dctype = this.getValueOfXmlNode(dcore, 'dc:type');
    let dcid = this.getValueOfXmlNode(dcore, 'dc:identifier');



    switch (dctype) {
      case 'Image':

        width = config.images_size;

        // Create the json of with the infos of the image
        json = "{";
        json += "\'ephoto_dam\':\'image\',";
        json += "\'align\' : \'" + config.align + "\',";
        json += "\'src\' : \'" + link + "\',";
        json += "\'alt\' : \'" + config.captions_format + "\',";
        json += "\'zoom\' : \'" + config.zoom + "\',";
        json += "\'captions\' : \'" + config.captions + "\',";
        json += "\'width\' : \'" + width + "\',";
        json += "\'id_image\' : \'" + dcid + "\',";
        json += "\'captions_format\' : \'" + config.captions_format + "\'";
        json += "}";

        imageSrc = '<img width="' + width + 'px" id="media_' + id + '" class="unselectable img_ephoto_dam ckeditor_ephoto_dam" data="'+json+'" src="' + link + '" alt="' + config.captions_format + '"/>';

        this.mediaHandler(imageSrc);

        break;

      default:
        var htmlObject = document.createElement('div');

        htmlObject.innerHTML = html;
        src = htmlObject.children[0].src;
        var url = new URL(src);

        width = url.searchParams.get("width");
        let height = url.searchParams.get("height");
        let title = htmlObject.children[0].title;

        // Get the image for the editing part
        let dcThumbnail = this.getValueOfXmlNode(dcore, 'dc:relation').replace("/small/", "/medium/");

        // Create the json of with the infos of the embeded object
        json = "{";
        json += "\'ephoto_dam\' : \'" + dctype + "\',";
        json += "\'align\' : \'" + config.align + "\',";
        json += "\'alt\' : \'" + config.captions_format + "\',";
        json += "\'zoom\' : \'" + config.zoom + "\',";
        json += "\'captions\' : \'" + config.captions + "\',";
        json += "\'height\' : \'" + height + "\',";
        json += "\'width\' : \'" + width + "\',";
        json += "\'id_image\' : \'" + dcid + "\',";
        json += "\'embed\' : \'" + src + "\',";
        json += "\'title\' : \'" + title + "\',";
        json += "\'captions_format\' : \'" + config.captions_format + "\'";
        json += "}";

        imageSrc = '<img width="' + width + 'px" id="media_' + id + '" class="unselectable ckeditor_ephoto_dam ' + dctype + '_ephoto_dam" data="'+json+'" src="' + dcThumbnail + '" alt="' + config.captions_format + '"/>';

        this.mediaHandler(imageSrc);

        break;
    }
  }










  /**
   * Get the link to the media
   * 
   * @param {string} auth_Id 
   * @param {string} id_media 
   * @param {*} dcore 
   * @param {*} html 
   */
  getlink(auth_Id, id_media, dcore, html) {

    if (id_media && auth_Id) {

      var urlInfo = drupalSettings.ephoto_dam.server_url + 'api/' + auth_Id + '/link/getToShare?files[]=' + id_media;

      var conn = new XMLHttpRequest();
      conn.open("GET", urlInfo);
      conn.send();
      var parser = new DOMParser();

      var self = this;
      conn.onreadystatechange = function () {
        if (conn.readyState === 4) {
          if (conn.status === 200) {

            var xmlDoc = parser.parseFromString(conn.responseText, "text/xml");

            var link = xmlDoc.querySelector('link').getAttribute("url");

            self.preparemedia(decodeURIComponent(link), dcore, html)

          }
        }
      }
    }
  }


  /**
  * generate a random string
  *
  * @param {int} length
  * @returns sttring
  */
  genRandonString(length) {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const charLength = chars.length;
    let result = '';
    for (let i = 0; i < length; i++) {
      result += chars.charAt(Math.floor(Math.random() * charLength));
    }
    return result;
  }

  /**
   * Add the media to the page
   *
   * @param {string} imageSrc
   */
  mediaHandler(imageSrc) {
    const viewFragment = this.editor.data.processor.toView(imageSrc);
    const modelFragment = this.editor.data.toModel(viewFragment);
    this.editor.model.insertContent(modelFragment);
  }


  /**
   * Returns the value of an xml node
   *
   * @param {tag} xml
   * @param {string} node
   * @returns
   */
  getValueOfXmlNode(xml, node) {
    return xml.getElementsByTagName(node)[0].textContent;
  }

  refresh() {
    const { model } = this.editor;
    const { selection } = model.document;

    // Determine if the cursor (selection) is in a position where adding a
    //  is permitted. This is based on the schema of the model(s)
    // currently containing the cursor.
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      'simpleBox',
    );

    // If the cursor is not in a location where it can be added, return
    // null so the addition doesn't happen.
    this.isEnabled = allowedIn !== null;
  }

}
