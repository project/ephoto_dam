/**
 * Ephoto Dam Addon for Drupal 9
 *
 * @base EPHOTO DAM
 * @author EINDEN
 */

(function () {
    var EphotoDam = {
        zoomIn: function (url) {

            var overlaydiv = document.createElement('div');
            overlaydiv.setAttribute('id', 'ephoto-dam-overlay');
            var overlay = document.querySelectorAll('#ephoto-dam-overlay').length ? document.querySelector('#ephoto-dam-overlay') : document.body.appendChild(overlaydiv);


            var wrap = document.createElement('div');
            wrap.setAttribute('id', 'ephoto-dam-wrap');
            var closeBtn = document.createElement('div');
            closeBtn.setAttribute('id', 'ephoto-dam-close');
            wrap.appendChild(closeBtn);
            var img2 = document.createElement('img');
            wrap.appendChild(img2);
            var wrap = document.querySelectorAll('#ephoto-dam-wrap').length ? document.querySelector('#ephoto-dam-wrap') : document.body.appendChild(wrap);
            wrap.style.opacity = 0;
            
            var buffer = new Image();

            buffer.onload = function () {
                var width, height;
                var ratiop = document.documentElement.clientHeight / document.documentElement.clientWidth;
                var ratiof = buffer.height / buffer.width;

                if (ratiop > ratiof) {
                    width = document.documentElement.clientWidth - 100;
                    height = Math.round(width * ratiof);
                }

                else {
                    height = document.documentElement.clientHeight - 100;
                    width = Math.round(height * (1 / ratiof));
                };

                var close = function (event) {
                    event.stopPropagation();
                    event.stopImmediatePropagation();

                    EphotoDam.fadeOut(overlay, 300,0.65);

                    // Utiliser setTimeout pour le délai
                    setTimeout(function () {
                        EphotoDam.fadeOut(wrap, 300,1); // Appel à la fonction d'animation
                    }, 250);
                    
                };



                // Trouve tous les enfants de `wrap`
                var children = wrap.children;

                // Sélectionne le dernier enfant
                var lastChild = children[children.length - 1];
                var firstChild = children[0];

                // Vérifie si le dernier enfant est bien une image avant de changer l'attribut 'src'
                if (lastChild && lastChild.tagName === 'IMG') {
                    lastChild.setAttribute('src', url);
                }

                // Vérifie si le premier enfant est bien une image avant de changer l'attribut 'src'
                if (lastChild && lastChild.tagName === 'IMG') {
                    lastChild.setAttribute('src', url);
                }

                if (firstChild) {
                    firstChild.addEventListener('click', close);
                }

                /*
                wrap.css({
                    'width': width,
                    'height': height,
                    'margin-left': -(Math.floor(width / 2) + 8),
                    'margin-top': -(Math.floor(height / 2) + 8)
                }).delay(250).fadeIn(300);
                */

                // Définir les styles CSS
                wrap.style.width = width + 'px';
                wrap.style.height = height + 'px';
                wrap.style.marginLeft = -(Math.floor(width / 2) + 8) + 'px';
                wrap.style.marginTop = -(Math.floor(height / 2) + 8) + 'px';





                // Utiliser setTimeout pour le délai
                setTimeout(function () {
                    EphotoDam.fadeIn(wrap, 300,1); // Appel à la fonction d'animation
                }, 250);




                // Appel à la fonction d'animation
                EphotoDam.fadeIn(overlay, 300,0.65);

                // Ajout de l'événement click
                overlay.addEventListener('click', close);

                //overlay.fadeIn(300).on('click', close);
            };

            buffer.src = url;
        },


        // Fonction pour afficher avec animation
        fadeIn: function (element, duration,limit) {

            /*
            var end = Date.now() + 1000
            while (Date.now() < end) continue
*/
            element.style.opacity = 0;
            element.style.display = 'block'; // Assurez-vous que l'élément est visible
/*
            end = Date.now() + 1000
            while (Date.now() < end) continue
*/

            var startTime = Date.now();



            function animate() {
                var elapsedTime = Date.now() - startTime;
                var progress = Math.min(elapsedTime / duration, 1); // Normalise le temps

                element.style.opacity = progress; // Décrémente l'opacité

                if (progress < limit) {
                    requestAnimationFrame(animate); // Continue l'animation
                }
            }

            requestAnimationFrame(animate); // Démarre l'animation
        },





        // Fonction pour faire disparaître l'élément avec animation
        fadeOut: function (element, duration,limit) {

            var startTime = Date.now();

            function animate() {
                var elapsedTime = Date.now() - startTime;
                var progress = Math.min(elapsedTime / duration, 1); // Normalise le temps
                
                element.style.opacity = limit - progress; // Décrémente l'opacité


                if (progress < limit) {
                    requestAnimationFrame(animate); // Continue l'animation
                } else {
                    element.style.display = 'none'; // Masque l'élément après l'animation
                }
            }

            requestAnimationFrame(animate); // Démarre l'animation
        },


    };



    const buttons = document.querySelectorAll('.ephoto-dam-zoom');

    buttons.forEach(button => {
        button.addEventListener('click', function(event) {
            event.stopPropagation();
            event.stopImmediatePropagation();
            var url = event.target.currentSrc;
            EphotoDam.zoomIn(url);
        });
    });




})();
